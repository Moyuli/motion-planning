function route = GradientBasedPlanner (f, start_coords, end_coords, max_its)
% GradientBasedPlanner : This function plans a path through a 2D
% environment from a start to a destination based on the gradient of the
% function f which is passed in as a 2D array. The two arguments
% start_coords and end_coords denote the coordinates of the start and end
% positions respectively in the array while max_its indicates an upper
% bound on the number of iterations that the system can use before giving
% up.
% The output, route, is an array with 2 columns and n rows where the rows
% correspond to the coordinates of the robot as it moves along the route.
% The first column corresponds to the x coordinate and the second to the y coordinate

[gx, gy] = gradient (-f);
position = start_coords;
route = position;
for k = 1:max_its
    if norm(position-end_coords) < 2
        break;
    end
    i = round(position(1));
    j = round(position(2));
    norm_gx = (gx(j,i)/(norm([gx(j,i), gy(j,i)])));
    norm_gy = (gy(j,i)/(norm([gx(j,i), gy(j,i)])));
    %norm_gx = (gx(j,i)/(gx(j,i).^2+gy(j,i).^2)^0.5);
    %norm_gy = (gy(j,i)/(gx(j,i).^2+gy(j,i).^2)^0.5);
    position(1) = position(1) + norm_gx;
    position(2) = position(2) + norm_gy;
    route = [route; position];    
end

% *******************************************************************
end
