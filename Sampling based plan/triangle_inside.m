function flag = triangle_inside(P1,P2)
flag = false;

y1 = P1(1,2);
y2 = P1(2,2);
y3 = P1(3,2);

y4 = P2(1,2);
y5 = P2(2,2);
y6 = P2(3,2);

x1 = P1(1,1);
x2 = P1(2,1);
x3 = P1(3,1);

x4 = P2(1,1);
x5 = P2(2,1);
x6 = P2(3,1);

if min(x1,min(x2,x3))<=x4 && x4<=max(x1,max(x2,x3)) && ...
   min(x1,min(x2,x3))<=x5 && x5<=max(x1,max(x2,x3)) && ...
   min(x1,min(x2,x3))<=x6 && x6<=max(x1,max(x2,x3)) && ...
   min(y1,min(y2,y3))<=y4 && y4<=max(y1,max(y2,y3)) && ...
   min(y1,min(y2,y3))<=y5 && y5<=max(y1,max(y2,y3)) && ...
   min(y1,min(y2,y3))<=y6 && y6<=max(y1,max(y2,y3))
    flag = true;
elseif min(x4,min(x5,x6))<=x1 && x1<=max(x4,max(x5,x6)) && ...
       min(x4,min(x5,x6))<=x2 && x2<=max(x4,max(x5,x6)) && ...
       min(x4,min(x5,x6))<=x3 && x3<=max(x4,max(x5,x6)) && ...
       min(y4,min(y5,y6))<=y1 && y1<=max(y4,max(y5,y6)) && ...
       min(y4,min(y5,y6))<=y2 && y2<=max(y4,max(y5,y6)) && ...
       min(y4,min(y5,y6))<=y3 && y3<=max(y4,max(y5,y6))
   flag = true;
end
end





