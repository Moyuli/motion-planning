function flag = line_intersection(x1,x2,x3,x4,y1,y2,y3,y4)
A1 = y2-y1;
B1 = x1-x2;
C1 = A1*x1+B1*y1;
A2 = y4-y3;
B2 = x3-x4;
C2 = A2*x3+B2*y3;
det = A1*B2 - A2*B1;
if (B2*C1 - B1*C2)==0 && (A1*C2 - A2*C1)==0
    flag = true;
elseif det == 0
    flag = false;
else 
    xx = (B2*C1 - B1*C2)/det;
    yy = (A1*C2 - A2*C1)/det;
    if min(x1,x2)<=xx && xx<=max(x1,x2) && min(y1,y2)<=yy && yy<=max(y1,y2)
        flag = true;
    else
        flag = false;
    end
end
end
