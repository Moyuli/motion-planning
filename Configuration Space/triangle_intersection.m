function flag = triangle_intersection(P1, P2)
% triangle_test : returns true if the triangles overlap and false otherwise

%%% All of your code should be between the two lines of stars.
% *******************************************************************
flag = false;

y2 = P1(2,2);
y1 = P1(1,2);
x2 = P1(2,1);
x1 = P1(1,1);

y4 = P2(2,2);
y3 = P2(1,2);
x4 = P2(2,1);
x3 = P2(1,1);
if line_intersection(x1,x2,x3,x4,y1,y2,y3,y4)
    flag = true;
end

y2 = P1(3,2);
y1 = P1(2,2);
x2 = P1(3,1);
x1 = P1(2,1);

y4 = P2(3,2);
y3 = P2(2,2);
x4 = P2(3,1);
x3 = P2(2,1);
if line_intersection(x1,x2,x3,x4,y1,y2,y3,y4)
    flag = true;
end

y2 = P1(1,2);
y1 = P1(3,2);
x2 = P1(1,1);
x1 = P1(3,1);

y4 = P2(1,2);
y3 = P2(3,2);
x4 = P2(1,1);
x3 = P2(3,1);
if line_intersection(x1,x2,x3,x4,y1,y2,y3,y4)
    flag = true;
end


if triangle_inside(P1,P2)
    flag = true;
end







% *******************************************************************
end